package atosApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.filechooser.FileSystemView;

import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualPictureProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPictureLocking;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTStretchInfoProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTDrawing;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTMarker;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTPicture;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTPictureNonVisual;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTTwoCellAnchor;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.STEditAs;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xslf.usermodel.XSLFRelation;

public final class ExcelSchedule {
	
	public static File getFilepath(){
		// Creates directory in Desktop called Clinician Appointment Data
		File home = FileSystemView.getFileSystemView().getHomeDirectory(); 			
		File dir = new File(home.getAbsolutePath()+"/Clinician Appointment Data");
		dir.mkdir();
	
		// Declare a file name with timestamp, adds file to directory stated in path and creates workbook
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	    df.setTimeZone(TimeZone.getTimeZone("GMT"));
		final File file = new File(home.getAbsolutePath()+"/Clinician Appointment Data/"+(df.format(new Date()))+".xlsx");
		return file;
	}
	
	public static File getFilepath2(){
		// Creates directory in Desktop called Clinician Appointment Data
		File home = FileSystemView.getFileSystemView().getHomeDirectory(); 			
		File dir = new File(home.getAbsolutePath()+"/Clinician Appointment Data");
		dir.mkdir();
	
		// Declare a file name with timestamp, adds file to directory stated in path and creates workbook
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	    df.setTimeZone(TimeZone.getTimeZone("GMT"));
		final File file = new File(home.getAbsolutePath()+"/Clinician Appointment Data/"+(df.format(new Date()))+"Unassigned.xlsx");
		return file;
	}
	
	public static void publishFile(XSSFWorkbook workbook, File filepath) throws FileNotFoundException, IOException{
		
		try (FileOutputStream outputStream = new FileOutputStream(filepath)){
			workbook.write(outputStream);
			outputStream.close();
		}
		workbook.close();
	}
	
	// Unassigned appointments list and overall success rates.
	public static void insertFinalStats(XSSFWorkbook workbook, ArrayList<PatientAppointment> unassigned,
											ArrayList<Double> successRates, long elapsedTime){
		
		XSSFSheet sheet = workbook.createSheet("Final statistics");
		
		Row successRateRow = sheet.createRow(0);
		Cell successRateTitle = successRateRow.createCell(0);
		successRateTitle.setCellValue("Overall Success");
		
		// Computes average success rate and prints this to the cell.
		Cell successRateFigure = successRateRow.createCell(2);
		Double sum = 0.0; Double averageSuccess = 0.0;
		  if(!successRates.isEmpty()) {
		    for (Double regionSuccess : successRates) {
		        sum += regionSuccess;
		    }
		    averageSuccess = sum.doubleValue() / successRates.size();
		  }
		successRateFigure.setCellValue(averageSuccess.intValue() + "%");
		
		Row elapsedTimeRow = sheet.createRow(sheet.getLastRowNum()+1);
		Cell time = elapsedTimeRow.createCell(0);
		time.setCellValue("Total elapsed time: ");
		Cell timeElapsed = elapsedTimeRow.createCell(2);
		timeElapsed.setCellValue(elapsedTime/1000 + " seconds");
		
		
		Row unassignedRow = sheet.createRow(sheet.getLastRowNum()+1);
		Cell unassignedCell = unassignedRow.createCell(0);
		unassignedCell.setCellValue("Unassigned Appointment IDs (" + unassigned.size() + ")");
		
		int size = unassigned.size();
		for (int i = 0; i < size; i++){
			Cell entry = unassignedRow.createCell(i+1);
			entry.setCellValue(unassigned.get(i).getID());
		}

	}

	public static void insertClinicianInfo(XSSFSheet sheet, Row row, Clinician clinician){
		
		//clinician ID in first column
		Cell name = row.createCell(0);
        name.setCellValue(clinician.getID());
        
        Cell slotCount = row.createCell(1);
        slotCount.setCellValue(clinician.getSlotCount());  
	}
	
	public static void insertString(XSSFSheet sheet, String string){
		
		int lastRow = sheet.getLastRowNum();
		Row row = sheet.createRow(lastRow + 1);
		Cell entry = row.createCell(0);
		entry.setCellValue(string);
	}
	
	public static void insertAppointments(Row row, ArrayList<Long> appointmentSequence, int sliceCount, boolean isTail){
		
		int startIndex = 0;
		if (isTail)
			startIndex = appointmentSequence.size()-sliceCount;
		
		for(int i = 0; i < sliceCount; i++){
			
        	Cell appointment = row.createCell(i+3);
	        appointment.setCellValue(appointmentSequence.get(i+startIndex).toString());
        }
	}
	
	public static <T> void insertUnassignedList(ArrayList<T> unassigned, XSSFSheet sheet){
		
		int rowCount = sheet.getLastRowNum();
		Row firstRow = sheet.createRow(rowCount+1);
		Cell cell = firstRow.createCell(0);
		cell.setCellValue("Unassigned appointment IDs: " + unassigned.toString());
	}
	
	public static void insertColumnTitles(XSSFSheet sheet){
		
		Row firstRow = sheet.createRow(0);
		Cell cell = firstRow.createCell(0);
        cell.setCellValue("Clinician ID");
        Cell cell1 = firstRow.createCell(1);
        cell1.setCellValue("Free slots");
		Cell cell2 = firstRow.createCell(3);
        cell2.setCellValue("Patient 1 ID");
		Cell cell3 = firstRow.createCell(4);
        cell3.setCellValue("Patient 2 ID");
		Cell cell4 = firstRow.createCell(5);
        cell4.setCellValue("Patient 3 ID");
	}
	
	private static void insertMapLegend(XSSFSheet sheet, ArrayList<PatientAppointment> optimisedPatientApptObjects){
		// Embeds URL of the static map containing the traversal sequence of the appointments (returned by the API)
		int size = optimisedPatientApptObjects.size();
		for (int i = 0; i < size; i++){
			Row row = sheet.getRow(i);
			if(row == null){
				row = sheet.createRow(i);
			}
			Cell keyCell = row.createCell(16);
			keyCell.setCellValue(String.valueOf((char)(i+65)));
			Cell postCodeCell = row.createCell(17);
			postCodeCell.setCellValue(optimisedPatientApptObjects.get(i).getPostCode());
			Cell idCell = row.createCell(18);
			idCell.setCellValue(optimisedPatientApptObjects.get(i).getID());
		}
	}
	
	public static void insertStaticMap(XSSFSheet sheet, ArrayList<PatientAppointment> optimisedPatientApptObjects){
		
		StaticMapAPI staticMap = new StaticMapAPI(optimisedPatientApptObjects);
		
		// Inserts legend for map - references every point on the map to the corresponding PatientAppointment ID and postcode.
		insertMapLegend(sheet, optimisedPatientApptObjects);
		
		// Retrieves URL of static map to embed into Excel sheet.
		String mapUrl = staticMap.getUrlString();
		
		// The comments below were here already. (From code online)
		// get drawing patriarch
		XSSFDrawing drawingPatriarch = sheet.createDrawingPatriarch();
		
		// get drawing object
		CTDrawing ctDrawing = drawingPatriarch.getCTDrawing();
		
		// create two cell anchor object
		CTTwoCellAnchor twoCellAnchor = ctDrawing.addNewTwoCellAnchor();
		twoCellAnchor.setEditAs( STEditAs.ONE_CELL );
		
		// create "from" element - hard-coded coordinates.
		CTMarker from = twoCellAnchor.addNewFrom();
		from.setCol( 9 );
		from.setColOff( 0 );
		from.setRow( 0 );
		from.setRowOff( 0 );
		
		// create "to" element
		CTMarker to = twoCellAnchor.addNewTo();
		to.setCol( 15 );
		to.setColOff( 355600 );
		to.setRow( 20 );
		to.setRowOff( 0 );
		
		// create "pic" element
		CTPicture pic = twoCellAnchor.addNewPic();
		
		// create "non visual pic pr" element
		CTPictureNonVisual nonVisPicPr = pic.addNewNvPicPr();
		
		// create "cNvPr" element
		CTNonVisualDrawingProps cnNvPr = nonVisPicPr.addNewCNvPr();
		cnNvPr.setId( 2 );
		cnNvPr.setName( "Graphical representation of optimal sequence" );
		cnNvPr.setDescr( "Optimal traversal sequence returned by API, static map generated by Google Static Maps API" );
		
		// create "cNvPicPr" element
		CTNonVisualPictureProperties cnVPicPr = nonVisPicPr.addNewCNvPicPr();
		
		// create "picLocks" element
		CTPictureLocking picLocks = cnVPicPr.addNewPicLocks();
		picLocks.setNoChangeAspect( true );
		
		// create "blipFill" element
		CTBlipFillProperties blipFill = pic.addNewBlipFill();
		
		// create "blip" element
		CTBlip blip = blipFill.addNewBlip();
		blip.setLink( "rId1" );
		
		// create "strech" element
		CTStretchInfoProperties strech = blipFill.addNewStretch();
		
		// create "fill rect" element
		strech.addNewFillRect();
		
		// create "spPr" element
		CTShapeProperties spPr = pic.addNewSpPr();
		
		// create "xfrm" element
		CTTransform2D xfrm = spPr.addNewXfrm();
		
		// create "off" element
		CTPoint2D off = xfrm.addNewOff();
		off.setX( 0 );
		off.setY( 0 );
		
		// create "ext" element
		CTPositiveSize2D ext = xfrm.addNewExt();
		ext.setCx( 4000000 );
		ext.setCy( 4000000 );
		
		// create "prstGeom" element
		CTPresetGeometry2D prstGeom = spPr.addNewPrstGeom();
		prstGeom.setPrst( STShapeType.RECT );
		
		// create "avLst" element
		prstGeom.addNewAvLst();
		
		// create "clientData" element
		twoCellAnchor.addNewClientData();
		
		// add image relation - IMAGE URL!
		drawingPatriarch.getPackagePart().addExternalRelationship(mapUrl, XSLFRelation.IMAGES.getRelation() );

	}
}
