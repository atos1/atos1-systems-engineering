package atosApp;

public class Clinician {
	private String id;
	private String postCode;
	private Schedule schedule;
	private String gender;
	
	public Clinician(String id, String postCode, String gender){
		this.id = id;
		this.postCode = postCode;
		this.gender = gender;
	}
	
	public void setSchedule(Schedule schedule){
		this.schedule = schedule;
	}
	
	public void removeSlots(int fromIndex, int toIndex){	
		schedule.removeSlots(fromIndex, toIndex);
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getPostCode(){
		return this.postCode;
	}
	
	public Schedule getSchedule(){
		return schedule;
	}
	
	public String getGender() {
		return this.gender;
	}
	
	public int getSlotCount(){
		return schedule.getSlotCount();
	}
	
	public long getSlotDuration(int i){
		return schedule.getSlotDuration(i);
	}
}
