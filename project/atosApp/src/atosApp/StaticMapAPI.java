package atosApp;

import java.util.ArrayList;


public class StaticMapAPI {
	
	// Generates image of route optimiser 
//	private final String exampleUrl = "http://maps.google.com/maps/api/staticmap?center=PL158NH&zoom=7&"
//			+ "size=300x300&markers=label:A%7CPL26%207PQ&markers=label:B%7CTR19%206HT"
//			+ "&markers=label:C%7CTR27%204JP&markers=label:D%7CTR6%200LL&markers=label:E%7CPL35%200AU"
//			+ "&markers=label:F%7CPL15%208NH&markers=label:G%7CEX14%201HB&markers=label:H%7CTQ3%203UU"
//			+ "&markers=label:I%7CTQ10%209YT&markers=label:J%7CTQ7%204DU&markers=label:K%7CPL21%200PS"
//			+ "|&path=weight:5|PL26%207PQ|TR19%206HT|TR27%204JP|TR6%200LL|PL35%200AU|PL15%208NH"
//			+ "|EX14%201HB|TQ3%203UU|TQ10%209YT|TQ7%204DU|PL21%200PS ";
	
	private StringBuilder urlRequest = new StringBuilder("http://maps.google.com/maps/api/staticmap?");
	private final String zoomSize = "7";
	private final String sizeDimension = "400x400";
	private ArrayList<PatientAppointment> optimisedPatientApptObjects;
	
	public StaticMapAPI(ArrayList<PatientAppointment> optimisedPatientApptObjects){
		
		this.optimisedPatientApptObjects = optimisedPatientApptObjects;
		addCentrePostCode();
		addZoomAndDimensions();
		addMarkers();
//		addPath(); 			currently not working 
	}

	private void addCentrePostCode(){
		
		int middleIndex = optimisedPatientApptObjects.size()/2;
		String center = optimisedPatientApptObjects.get(middleIndex).getPostCode();
		this.urlRequest.append("center=" + center.replaceAll("\\s+",""));
	}
	
	private void addZoomAndDimensions(){
		
		this.urlRequest.append("&zoom=" + zoomSize + "&size=" + sizeDimension);
	}
	
	private void addMarkers(){
		
		int size = optimisedPatientApptObjects.size();
		for (int i = 0; i < size; i++){
			this.urlRequest.append("&markers=label:" + (char)(i+65) + "%7C" 
				+ optimisedPatientApptObjects.get(i).getPostCode());
			
		}
		
	}
	
//	private void addPath(){
//		
//		this.urlRequest.append("|&path=weight:5");
//		int size = optimisedSequence.size();
//		for (int i = 0; i < size; i++){
//			this.urlRequest.append("|" + appointmentsInRegion.get(optimisedSequence.get(i)).getPostCode());
//		}
//		
//	}
	
	public String getUrlString(){
		return this.urlRequest.toString().replaceAll("\\s+","");
	}


}
