package atosApp;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class RouteOptimiserAPI {
	
	String apiResponse = null;
	private HashMap<Integer, PatientAppointment> unoptimisedPatientApptObjects = new HashMap<Integer, PatientAppointment>();
	ArrayList<PatientAppointment> optimisedPatientApptObjects = new ArrayList<PatientAppointment>();
	ArrayList<Integer> apiLocationSequence = new ArrayList<Integer>();
	ArrayList<Long> IDSequence = new ArrayList<Long>();
	ArrayList<LocalTime> journeyTimes = new ArrayList<LocalTime>();
		
	public abstract void executeApiCall(String jsonParameter) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;
	
	public void setApiResponse(String apiResponse){
		this.apiResponse = apiResponse;
	}

	public void setApiLocationSequence(ArrayList<Integer> apiLocationSequence){
		this.apiLocationSequence = apiLocationSequence;
	}
	
	// Returns patients in region in optimised sequence order.
	public ArrayList<PatientAppointment> getOptimisedPatientApptObjects(){
		return this.optimisedPatientApptObjects;
	}
		
	
	// Sets ID sequence
	public void setPatientAppointmentSequence(){
		
		ArrayList<Long> results = new ArrayList<Long>();
		int length = getApiLocationSequence().size();
		
		for(int i = 0; i < length; i++){
			int current = getApiLocationSequence().get(i);
			results.add(Long.parseLong(getUnoptimisedPatientApptObjects().get(current).getID()));
			this.optimisedPatientApptObjects.add(getUnoptimisedPatientApptObjects().get(current));
		}
		this.IDSequence = results;
	}

	public void setJourneyTimes(ArrayList<LocalTime> journeyTimes){
		this.journeyTimes = journeyTimes;
	}
	
	public String getApiResponse(){
		return this.apiResponse;
	}
	
	public HashMap<Integer, PatientAppointment> getUnoptimisedPatientApptObjects(){
		return this.unoptimisedPatientApptObjects;
	}
	
	public ArrayList<Integer> getApiLocationSequence(){
		return this.apiLocationSequence;
	}
	
	public ArrayList<Long> getIDSequence(){
		return this.IDSequence;
	}

	public ArrayList<LocalTime> getJourneyTimes(){
		return this.journeyTimes;
	}
		
	public abstract ArrayList<Integer> extractLocationSequence (String apiResponse);
	
	public abstract ArrayList<LocalTime> extractTravelTimes (String apiResponse);
}
