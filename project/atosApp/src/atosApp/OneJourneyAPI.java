package atosApp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.time.LocalTime;

public class OneJourneyAPI {
	
	private String urlRequest = "https://maps.googleapis.com/maps/api/directions/json?";
	private String API_KEY = "&key=AIzaSyDpnYwaZeWKku0S6trpfBf6siityLOG92s";
//	private String API_KEY = "&key=AIzaSyBtoDSYy5DbSwXSsdRSw6DwM2U_9GntAcA";
	private String clinicianPostCode;
	private String patientPostCode;
	private String apiResponse; 
	private LocalTime journeyTime;
	
	public OneJourneyAPI(String clinicianPostCode, String patientPostCode) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		this.clinicianPostCode = clinicianPostCode.replaceAll("\\s+","");
		this.patientPostCode = patientPostCode.replaceAll("\\s+","");
		this.urlRequest = urlRequest + "origin=" + this.clinicianPostCode + "&destination=" + this.patientPostCode + this.API_KEY;
		executeApiCall();
		extractJourneyTime();
		System.out.println(journeyTime);
	}
	
	// Executes api call with json parameter and returns location sequence.
	public void executeApiCall() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
	      
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		
		try {
			URL url = new URL(this.urlRequest);
			urlConn = url.openConnection();
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(),
						Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					String cp;
					while ((cp = bufferedReader.readLine()) != null) {
						sb.append(cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:"+  e);
		} 
				      	      
		this.apiResponse = sb.toString();
	}
	
	
	private void extractJourneyTime(){

		String temp = apiResponse;
		int i = temp.indexOf("\"duration\"");
		int j = temp.indexOf("\"value\"", i);
		int k = temp.indexOf("}", j);
		try {
			String value = (temp.substring(j, k));
			value = value.replaceAll("\\s+","");
			value = value.substring(8);
			int apiSeconds = Integer.parseInt(value);
			int seconds = (int) apiSeconds % 60 ;
			int minutes = (int) ((apiSeconds / 60) % 60);
			int hours   = (int) ((apiSeconds / (60*60)) % 24);
			this.journeyTime = LocalTime.of(hours, minutes, seconds);
		}
		catch (Exception e){
			System.out.println("Api call failed: " + urlRequest);
			System.out.println(apiResponse);
			// need to find out when the index is -1!!
		}
	}
	
	
	public LocalTime getJourneyTime(){
		return this.journeyTime;
	}

}
