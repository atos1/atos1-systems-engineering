package atosApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

public final class PrintStatement {
	
	public static void cliniciansInRegion(ArrayList<Clinician> cliniciansInRegion){
		
		System.out.print("Clinicians in region: ");
		
		for (Clinician clinician : cliniciansInRegion)
			System.out.print("(" + clinician.getID() + ", " + clinician.getPostCode() + ") "); // Removable
		System.out.println(); // Removable
		
	}
	
	public static void appointmentsInRegion(ArrayList<PatientAppointment> patientsInRegion){
		
		System.out.print("Patients in region: ");
		
		for (PatientAppointment patient : patientsInRegion)
			System.out.print("(" + patient.getID() + ", " + patient.getPostCode() + ") "); // Removable
		System.out.println(); // Removable
		
	}
	
	public static void regionsPerBatch(ArrayList<Integer> regionsInPatientBatch, ArrayList<Integer> regionsInClinicianBatch){
		
		System.out.println("Regions in patient/appointments batch: " + regionsInPatientBatch);
		System.out.println("Regions in clinicians: " + regionsInClinicianBatch);
		System.out.println();
		
	}
	
	public static void appointmentIDs(ArrayList<PatientAppointment> unassignedAppointments){
		
		System.out.print("Unassigned appointment IDs: ");
		for (PatientAppointment appointment : unassignedAppointments){
			System.out.print(appointment.getID() + " ");
		}
		System.out.println();
		
	}
	
	public static void clinicianSlots(ArrayList<Clinician> cliniciansInRegion){
		
		for (Clinician clinician : cliniciansInRegion){
			System.out.println(clinician.getID() + " free slots: " + clinician.getSlotCount());
		}
	}
	
	public static void ClinicianAllRegionSlots(HashMap<Integer, ArrayList<Clinician>> clinicianMap){
		
		final Set<Entry<Integer, ArrayList<Clinician>>> clinicianMapEntries = clinicianMap.entrySet();
		for (Entry<Integer, ArrayList<Clinician>> cliniciansInRegion : clinicianMapEntries) {
			System.out.println("Region " + cliniciansInRegion.getKey());
			clinicianSlots(cliniciansInRegion.getValue());
		}
	}

}
