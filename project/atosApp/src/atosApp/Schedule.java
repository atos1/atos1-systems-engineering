package atosApp;

import java.time.temporal.ChronoUnit;
import java.time.LocalTime;
import java.util.ArrayList;

public class Schedule {

	private ArrayList<LocalTime> slotStartTimes;
	private ArrayList<LocalTime> slotEndTimes;
	
	public Schedule() {
		slotStartTimes = new ArrayList<LocalTime>();
		slotEndTimes = new ArrayList<LocalTime>();
	}
	
	public void addTimeSlot(LocalTime startTime, LocalTime endTime) {
		slotStartTimes.add(startTime);
		slotEndTimes.add(endTime);
	}
	
	public long getSlotDuration(int i){
		return this.slotStartTimes.get(i).until(slotEndTimes.get(i), ChronoUnit.MINUTES);
	}
		
	public int getSlotCount() {
		return slotStartTimes.size();
	}
	
	public void removeSlots(int fromIndex, int toIndex){
		
		this.slotStartTimes.subList(fromIndex, toIndex).clear();
		this.slotEndTimes.subList(fromIndex, toIndex).clear();
	}

	
}


/*after you assign a schedule to a clinician, we will have a for loop that goes through all the clinicians in the region and looks for the ones with 3 free slots
then it goes about slicing 3 from the location sequence
then once thats done with
we will have another for loop going through te clinicians with 2 free slots
and slice 2 from the location sequence...
and repeat the same with clinicians with just 1 free slot
correction* to the first thing i wrote
i meant to write
after you assign a schedule to every* clinician
and we might have to do some calculations with the time ranges
i.e. checking how long it takes to get from one place to another 


Use LocalTime of(int hours, int minutes) to specify time
max number of slots = 3

?
 */