package atosApp;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class ScheduleGenerator {
	
	private static ArrayList<Schedule> allSchedules;
	
	public static void assignAllSchedules(HashMap<Integer, ArrayList<Clinician>> allClinicians){
		Map<Integer, ArrayList<Clinician>> allCliniciansMap = allClinicians;
		for (Entry<Integer, ArrayList<Clinician>> cliniciansInRegion : allCliniciansMap.entrySet()) {
		    assignSchedulesToRegion(cliniciansInRegion.getValue());
		}
	}
	
	private static void assignSchedulesToRegion(ArrayList<Clinician> cliniciansInRegion){
		
		for (Clinician clinician : cliniciansInRegion){
			clinician.setSchedule(getRandomSchedule());
//			System.out.println(clinician.getID() +" has " + clinician.getSlotCount());
		}
	}
	
	public static Schedule getRandomSchedule(){
		return allSchedules.get((int)(Math.random()*9));
	}

	
	//these slots should be at least two hours
	static {
		
		allSchedules = new ArrayList<Schedule>(); //array list of schedules to be used for assignments
		
		//create schedules and add to list of schedules in preparation for assignment to clinicians
		Schedule s1 = new Schedule(); //no free slots
		s1.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s1.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s1.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));
		allSchedules.add(s1);
		
		Schedule s2 = new Schedule(); //two free slots
		s2.addTimeSlot(LocalTime.of(10,0,0), LocalTime.of(12,30,0));
		s2.addTimeSlot(LocalTime.of(14,5,0), LocalTime.of(16,40,0));
		allSchedules.add(s2);
		
		Schedule s3 = new Schedule();//three free slots
		s3.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s3.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s3.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));
		allSchedules.add(s3);
		
		Schedule s4 = new Schedule();//one free slot
		s4.addTimeSlot(LocalTime.of(15,30,0), LocalTime.of(18,0,0));
		allSchedules.add(s4);
		
		Schedule s5 = new Schedule();
		s5.addTimeSlot(LocalTime.of(9,0,0), LocalTime.of(11,30,0));
		s5.addTimeSlot(LocalTime.of(13,45,0), LocalTime.of(15,45,0));
		allSchedules.add(s5);
		
		Schedule s6 = new Schedule(); 
		s6.addTimeSlot(LocalTime.of(8,30,0), LocalTime.of(11,0,0));
		s6.addTimeSlot(LocalTime.of(12,30,0), LocalTime.of(14,45,0));
		s6.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,30,0));
		allSchedules.add(s6);
		
		Schedule s7 = new Schedule();
		s7.addTimeSlot(LocalTime.of(8,30,0), LocalTime.of(11,0,0));
		s7.addTimeSlot(LocalTime.of(12,30,0), LocalTime.of(14,45,0));
		s7.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,30,0));
		allSchedules.add(s7);
		
		Schedule s8 = new Schedule();
		s8.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,0,0));
		s8.addTimeSlot(LocalTime.of(13,45,0), LocalTime.of(16,35,0));
		allSchedules.add(s8);
		
		Schedule s9 = new Schedule();
		s9.addTimeSlot(LocalTime.of(12,0), LocalTime.of(14,30,0));
		allSchedules.add(s9);
		
		Schedule s10 = new Schedule();
		s10.addTimeSlot(LocalTime.of(9,0,0), LocalTime.of(11,30,0));
		s10.addTimeSlot(LocalTime.of(12,35), LocalTime.of(15,0,0));
		allSchedules.add(s10);
	}
	
}
