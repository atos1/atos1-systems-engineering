package atosApp;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.time.temporal.ChronoUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class AppointmentScheduler {
	
	private final double appointmentDuration = 90.0;
	private final double extraTimeForFirstAppointment = 30.0; // for travelling to first appointment
	private ArrayList<Long> appointmentSequenceIDs; 
	private ArrayList<Integer> appointmentSequenceIndex;
	private ArrayList<PatientAppointment> unassignedAppointments;
	private ArrayList<PatientAppointment> unoptimisedPatientApptObjects; // can remove this but will require to change StaticMap API class
	private ArrayList<PatientAppointment> optimisedPatientApptObjects; // instead we can simply just use this to reference. 
	private ArrayList<Clinician> cliniciansInRegion;
	private ArrayList<LocalTime> journeyTimes;
	private RouteOptimiserAPI result; 
	private XSSFWorkbook workbook;
	private XSSFSheet currentSheet;
	private Row currentRow;
	private double successRate;
		
	// For regions with Clinicians
	public AppointmentScheduler(ArrayList<Clinician> cliniciansInRegion, ArrayList<PatientAppointment> appointmentsInRegion, 
								XSSFWorkbook workbook, RouteOptimiserAPI result) // APICall result
										throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		PrintStatement.cliniciansInRegion(cliniciansInRegion); 
		this.cliniciansInRegion = cliniciansInRegion;
		this.unoptimisedPatientApptObjects = appointmentsInRegion;
		this.result = result;
		this.journeyTimes = result.getJourneyTimes();
		this.workbook = workbook;
		
		// Retrieving "optimal" sequence to traverse appointments in region
		// Appointment sequence identifiable by ID (from SQL table)
		
		// i.e. [ 2 == 10384, 4 == 10331, 6 == 10232, 1 == 13492, 3 == 12313, 5 == 12309 ]
		this.appointmentSequenceIDs = this.result.getIDSequence();
		// i.e. [2,4,6,1,3,5]
		this.appointmentSequenceIndex = this.result.getApiLocationSequence();
		// optimised order of PatientAppointment objects
		this.optimisedPatientApptObjects = this.result.getOptimisedPatientApptObjects();
		
		// Assigns schedule and prints to excel sheet
		scheduler(this.appointmentSequenceIDs); 
		
		// Setting unassigned appointments to object's (region's) unassignedAppointments list
		setUnassignedList();
	}
	
	// For regions with no Clinicians
	public AppointmentScheduler(ArrayList<PatientAppointment> appointmentsInRegion, int currentRegion, XSSFWorkbook workbook){
		
		this.unoptimisedPatientApptObjects = appointmentsInRegion;
		this.workbook = workbook;
		
		// Creates new sheet for region.
		this.currentSheet = workbook.createSheet("Region " + currentRegion);	
		ArrayList<String> unassigned = new ArrayList<String>();
		for (PatientAppointment appointment : appointmentsInRegion){
			// Adds all the PatientAppointments in the region to a list of unassigned appointments.
			unassigned.add(appointment.getID());
		}
		// Prints unassigned appointments to the sheet for the current region.
		ExcelSchedule.insertUnassignedList(unassigned, this.currentSheet);
		this.unassignedAppointments = appointmentsInRegion;
	}
	
	
	
	private void scheduler(ArrayList<Long> appointmentSequence) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		// Retrieves current region value.
		Integer region = LookupTable.lookupTable.get(Region.getPostCodePrefix(this.cliniciansInRegion.get(0).getPostCode()));
		
		// Sets Excel sheet name
		this.currentSheet = this.workbook.createSheet("Region " + region);	
		
		// Prints out all appointments in current region to console.
		PrintStatement.appointmentsInRegion(unoptimisedPatientApptObjects); 
		
		// Prints out "Clinician ID | Patient 1 | Patient 2 | Patient 3" titles to Excel sheet.
		ExcelSchedule.insertColumnTitles(this.currentSheet);
		
		// Embeds static map URL to Excel sheet.
		ExcelSchedule.insertStaticMap(this.currentSheet, optimisedPatientApptObjects);
		
		int clinicianRow = 1;
		
		// Records the start time of execution - for working out total scheduling execution time.
	    long startTime = System.currentTimeMillis();
	    
	    // Records total appointments and total Clinician free slots
	    // - for working out "success rate"
	    int totalAppointments = appointmentSequence.size();
	    int totalFreeSlots = 0;
	    for(Clinician clinician: this.cliniciansInRegion){
	    	totalFreeSlots += clinician.getSlotCount();
	    }
	    System.out.println(this.cliniciansInRegion.size());
	

		// Slicing 3, 2 or 1 appointments from "optimal sequence" to clinicians 
		for(int sliceCount = 3; sliceCount > 0; sliceCount--){
			
			for(Clinician clinician : this.cliniciansInRegion ){
				
				// If the number of appointments is less than the sliceCount value - 
				// break out of inner for loop - in order to skip to the next sliceCount iteration in the outer for loop
				// Reason: You cannot slice more appointments than there are.
				if(this.appointmentSequenceIDs.size() < sliceCount) {
					break;
				}
				
			    if(clinician.getSlotCount() == sliceCount) {
			    	
			    	// Sets the current row of the Excel sheet.
			    	this.currentRow = this.currentSheet.getRow(clinicianRow) != null ? 
			    			this.currentSheet.getRow(clinicianRow++) : this.currentSheet.createRow(clinicianRow++);

			    	// Prints Clinician info to the Excel sheet.
			    	ExcelSchedule.insertClinicianInfo(this.currentSheet, this.currentRow, clinician);
			    	
			    	// Schedules (slightly differently) according to the sliceCount number.
			    	// General principle that is applied to all cases:
			    	// 		It is checked that the Clinician is able to travel to every single appointment from the sliceCount amount from:
			    	//		the head of the appointment sequence and if not then the tail of the appointment sequence.
			    	switch(sliceCount){
			    		case 1: oneSlot(clinician, null); break;
			    		case 2: twoSlots(clinician); break;
			    		case 3: threeSlots(clinician); break;
			    		default: break;
			    	}
						    	
			    }
			}
		}
		ArrayList<Long> unassignedAppointments = this.appointmentSequenceIDs;
		System.out.println();
		
		// Prints unassigned appointments to current region's Excel sheet.
		ExcelSchedule.insertUnassignedList(unassignedAppointments, this.currentSheet);
		
		// Computing "success rate":
		int totalAssigned = totalAppointments - unassignedAppointments.size();
		ExcelSchedule.insertString(this.currentSheet, "Total appointments: " + totalAppointments);
		ExcelSchedule.insertString(this.currentSheet, "Total free slots: " + totalFreeSlots);
		ExcelSchedule.insertString(this.currentSheet, "Total assigned: " + totalAssigned);
		
		// Initially simply set to: Total assigned / Total Clinician free slots.
		this.successRate = (double)(totalAssigned*100.0f/totalFreeSlots);
		
		// Set to: Total assigned / Total appointments only if Total Free slots exceed Total appointments
		// Necessary as Clinician free slots maybe higher than total appointments,
		// So if there are 30 free slots but of 10 appointments, 9 have been assigned the success rate would have to cater for this.
		// (33.3% in this case would not make any sense).
		if (totalFreeSlots > totalAppointments)
			this.successRate = (double)totalAssigned*100.0f/totalAppointments;
		
		if (Double.isNaN(this.successRate))
			this.successRate = 0.0;
		
	    long elapsedTime = System.currentTimeMillis() - startTime;
	    
	    // Prints success rate and elapsed time to Excel sheet.
	    ExcelSchedule.insertString(this.currentSheet, "Success percentage: " + this.successRate + "%");
	    ExcelSchedule.insertString(this.currentSheet, "Total time elapsed: " + elapsedTime);
	}

	
	private void threeSlots(Clinician clinician) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		LocalTime timeToFirstAppointment = clinicianToPatientTime(clinician);
		
		if (checkFirstAppointments(3, clinician, 0, timeToFirstAppointment)){
			// 3 slots - front of sequence & check 1st, 2nd and 3rd clinician slots
			ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 3, false);
			updateSequenceLists(0,3);
		}
		else if (checkLastAppointments(3, clinician, 0)){
			// 3 slots - back of sequence & check 1st, 2nd and 3rd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 3, true);	
			updateSequenceLists(appointmentSequenceIDs.size()-3, appointmentSequenceIDs.size());
		}
		else if (checkFirstAppointments(2, clinician, 0, timeToFirstAppointment)){
			// 2 slots - front of sequence & check 1st and 2nd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 2, false);
			updateSequenceLists(0,2);
		}
		else if (checkFirstAppointments(2, clinician, 1, timeToFirstAppointment)){
			// 2 slots - front of sequence & check 2nd and 3rd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 2, false);
			updateSequenceLists(0,2);
		}
		else if (checkLastAppointments(2, clinician, 0)){
			// 2 slots - back of sequence & check 1st and 2nd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 2, true);	
			updateSequenceLists(appointmentSequenceIDs.size()-2, appointmentSequenceIDs.size());
		}
		else if (checkLastAppointments(2, clinician, 1)){
			// 2 slots - back of sequence & check 2nd and 3rd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 2, true);	
			updateSequenceLists(appointmentSequenceIDs.size()-2, appointmentSequenceIDs.size());
		}
		else {
			oneSlot(clinician, timeToFirstAppointment);
		}
	}
	
	private void twoSlots(Clinician clinician) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		LocalTime timeToFirstAppointment = clinicianToPatientTime(clinician);
		
		if (checkFirstAppointments(2, clinician, 0, timeToFirstAppointment)){
			// 2 slots - front of sequence & check 1st and 2nd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 2, false);
			updateSequenceLists(0,2);
		}
		else if (checkLastAppointments(2, clinician, 0)){
			// 2 slots - back of sequence & check 1st and 2nd clinician slots
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 2, true);	
			updateSequenceLists(appointmentSequenceIDs.size()-2, appointmentSequenceIDs.size());
		}
		else {
			oneSlot(clinician, timeToFirstAppointment);
		}
		
	}
	
	private void oneSlot(Clinician clinician, LocalTime timeToAppointment) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		if ( timeToAppointment == null ) {
			timeToAppointment = clinicianToPatientTime(clinician);
		}

		if (checkFirstAppointments(1, clinician, 0, timeToAppointment)) {
			// 1 slot - front of sequence & checks the Clinician's only slot
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 1, false);
			updateSequenceLists(0,1);
		} 
		else if (checkLastAppointments(1, clinician, 0)) {
			// 1 slot - back of sequence & checks the Clinician's only slot
	        ExcelSchedule.insertAppointments(this.currentRow, this.appointmentSequenceIDs, 1, true);	
			updateSequenceLists(appointmentSequenceIDs.size()-1, appointmentSequenceIDs.size());
		}
	}
	
	// Extra parameter for time taken to get to first appointment in sequence.
	private boolean checkFirstAppointments(int sliceNo, Clinician clinician, 
										int scheduleStartIndex, LocalTime timeToFirstAppointment){
		
		if (this.journeyTimes.isEmpty()){
			return false;
		}
		
		// Time taken to get to first appointment in remaining appointment sequence
		// This should be passed into checkFirstAppointments() method to avoid exact duplicate API calls.
		
		for (int i = 0; i < sliceNo; i++){

			double travelDuration = LocalTime.MIDNIGHT.until(this.journeyTimes.get(i), ChronoUnit.MINUTES);
   			if ( i == 0 ){
   				travelDuration = LocalTime.MIDNIGHT.until(timeToFirstAppointment, ChronoUnit.MINUTES);
   			}
	    	double slotDuration = clinician.getSlotDuration(i+scheduleStartIndex);
	    	
	    	if (appointmentDuration + travelDuration > slotDuration){
	    		return false;
	    	}
	    	
	    	// Checking gender preferences. If they have same gender preferences, checks if both patient and clinician
	    	// are the same sex in order to return true.
	    	String genderPref = this.optimisedPatientApptObjects.get(i).getPref();
	    	if ( genderPref != null ){
	    		if ( optimisedPatientApptObjects.get(i).getGender().equals(clinician.getGender()) == false)
	    			return false;
	    	}
	    	
		}
		return true;
	}
	
	private boolean checkLastAppointments(int sliceNo, Clinician clinician, int scheduleStartIndex){
		
		// Calculate how long it takes for the clinician to get to the first schedule.
		int startIndex = this.journeyTimes.size() - sliceNo;
		int endIndex = startIndex + sliceNo;
		int sizeOfOptimisedList = this.optimisedPatientApptObjects.size();
		
		// Time to get to the first appointment in the sliced tail of remaining appointment sequence
		LocalTime timeToFirstAppointment = LocalTime.of(0, 0, 0);
		try {
			timeToFirstAppointment = clinicianToPatientTime(clinician, sliceNo);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} 
		
		for (int i = startIndex, j = 0; i < endIndex; i++, j++){
			
   			double travelDuration = LocalTime.MIDNIGHT.until(this.journeyTimes.get(i), ChronoUnit.MINUTES);
	    	double slotDuration = clinician.getSlotDuration(j+scheduleStartIndex);

   			if ( i == startIndex ){
   				// Time taken to get to the first of appointment slice.
   				travelDuration = LocalTime.MIDNIGHT.until(timeToFirstAppointment, ChronoUnit.MINUTES);
   				slotDuration += extraTimeForFirstAppointment;
   			}
	    	
	    	if (appointmentDuration + travelDuration > slotDuration){
	    		return false;
	    	}

	    	// Checking gender preferences. If they have same gender preferences, checks if both patient and clinician
	    	// are the same sex in order to return true.
	    	int k = sizeOfOptimisedList - sliceNo + j;
	    	String genderPref = this.optimisedPatientApptObjects.get(k).getPref();
	    	if ( genderPref != null ){
	    		if ( optimisedPatientApptObjects.get(k).getGender().equals(clinician.getGender()) == false)
	    			return false;
	    	}
	    	
		}
		return true;
	}

	
	private void setUnassignedList(){
		
		ArrayList<PatientAppointment> unassignedAppts = new ArrayList<PatientAppointment>();
		int size2 = this.appointmentSequenceIDs.size();
		
		for(int i = 0; i < size2; i++){ // iterating through each appointment in the unassigned appointment sequence
			for (int j = 0; j < this.unoptimisedPatientApptObjects.size(); j++){
				if (Long.parseLong(unoptimisedPatientApptObjects.get(j).getID()) == this.appointmentSequenceIDs.get(i))
					unassignedAppts.add(unoptimisedPatientApptObjects.get(j));
			}
		}
		
		this.unassignedAppointments = unassignedAppts;
	}
	
	
	public ArrayList<PatientAppointment> getUnassignedAppointments(){
		
		return this.unassignedAppointments;
	}
	
	
	private void updateSequenceLists(int startIndex, int endIndex){
		// Removes assigned appointments from sequence lists.
		this.appointmentSequenceIDs.subList(startIndex, endIndex).clear();
		this.appointmentSequenceIndex.subList(startIndex, endIndex).clear();
		this.optimisedPatientApptObjects.subList(startIndex, endIndex).clear();
	}
	
	
	private LocalTime clinicianToPatientTime(Clinician clinician) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		// Time taken for clinician to reach the first appointment at the front of appointment sequence
		System.out.println("Start of appointment sequence");
		String clinicianPostCode = clinician.getPostCode();
		int index = this.appointmentSequenceIndex.get(0);
		String patientPostCode = unoptimisedPatientApptObjects.get(index).getPostCode();

		OneJourneyAPI oneJourney = new OneJourneyAPI(clinicianPostCode, patientPostCode);
		return oneJourney.getJourneyTime();
	}
	
	
	private LocalTime clinicianToPatientTime(Clinician clinician, int sliceNo) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		// Time taken for clinician to reach the first appointment in sliced tail of appointment sequence
		System.out.println("Tail of appointment sequence");
		String clinicianPostCode = clinician.getPostCode();
		int index = this.appointmentSequenceIndex.get(appointmentSequenceIndex.size()-sliceNo);
		String patientPostCode = unoptimisedPatientApptObjects.get(index).getPostCode();
		
		OneJourneyAPI oneJourney = new OneJourneyAPI(clinicianPostCode, patientPostCode);
		return oneJourney.getJourneyTime();
	}
	
	public double getSucessRate(){
		
		return this.successRate;
	}
	
	/* Check if slot duration is greater than travelling time + appointment duration
	   Only slices consecutive slots to maintain location sequence. 
	   
	   Checks how much of the sliceCount can actually fit in to the clinician's schedule,
	   according to slot duration.
	   
	*/

	
	/* Alternative method, (hopefully, non-buggy method):
	 * for the "condition" to be satisfied, 90 minutes + travel time (minutes) <= slot duration (minutes)
	 * 
	 * For when sliceCount == 3 for instance,
	 * 1a. check if 1st, 2nd and 3rd slots satisfy condition for first 3 appointment sequence indexes.
	 * 1b. if it fails check if slots satisfy condition for last 3 appointment sequence indexes.
	 * 1c. if it still fails, check if 1st and 2nd *or* 2nd and 3rd slot satisfy condition for first 2 appointment indexes
	 * 1d. if it still fails, check if 1st and 2nd *or* 2nd and 3rd slot satisfy condition for last 2 appointment indexes 
	 * 1e. if it still fails, check if 1st or 3rd slot satisfy condition for first appointment index
	 * 1f. if it still fails, check if 1st or 3rd slot satisfy condition for last appointment index
	 * 
	 * 
	 * For when sliceCount == 2,
	 * 2a. check if 1st and 2nd slots satisfy condition for first 2 appointment sequence indexes.
	 * 2b. if it fails check if slots satisfy condition for last 2 appointment sequence indexes.
	 * 2c. if it still fails, check if 1st or 2nd slot satisfy condition for first appointment index.
	 * 2d. if it still fails, check if 1st or 2nd slot satisfy condition for last appointment index
	 * 
	 * 
	 * For when sliceCount == 1,
	 * 3a. check if the only slot satisfies condition for first appointment sequence index.
	 * 3b. if it still fails, check if slot satisfies condition for last appointment index.
	 * 
	 * 
	 * For whatever sliceCount, if a condition holds, slice the corresponding indexes from the appointment sequence
	 * And remove the corresponding slots from the clinician's schedule.
	 * - Removing from head of appointmentSequence:
	 *   appointmentSequence.subList(0, sliceAmount).clear();
	 * - Removing from tail of appointmentSequence:
	 *   appointmentSequence.subList(appointmentSequence.size()- sliceAmount, appointmentSequence.size()).clear();
	 *   
	 * - Removing slots from clinician's schedule:
	 * 	 clinician.removeSlots(fromIndex, toIndex); parameters for each case:
	 *   (fromIndex is inclusive, toIndex is exclusive)
	 * for 1a/b: (0,3),   1c/d: (0,2) or (1,3),   1e/f: (0,1) or (2,3)
	 * for 2a/b: (0,2),   2c/d: (0,1) or (1,2)
	 * for 3a/b: (0,1)
	 */


}
