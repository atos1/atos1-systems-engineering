package atosApp;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class LookupTable {
	
	public static final Map<String, Integer> lookupTable;
	
	static{
		HashMap<String, Integer> aMap = new HashMap<String, Integer>();
        aMap.put("PL", 1);
        aMap.put("TQ", 1);
        aMap.put("TR", 1);
        aMap.put("EX", 1);
        
        aMap.put("TA", 2);     
        aMap.put("DT", 2);
        aMap.put("BS", 2);
        aMap.put("BA", 2);
        
        aMap.put("SP", 3);
        aMap.put("BH", 3);
        aMap.put("SO", 3);
        aMap.put("PO", 3);
        
        aMap.put("BN", 4);
        aMap.put("RH", 4);
        aMap.put("TN", 4);
        aMap.put("CT", 4);
        
        aMap.put("ME", 5);
        aMap.put("SS", 5);
        aMap.put("CM", 5);     
        aMap.put("RM", 5);
        aMap.put("IG", 5);
        
        aMap.put("RG", 6); 
        aMap.put("SL", 6);
        aMap.put("GU", 6);
        aMap.put("HP", 6);
        
        aMap.put("SN", 7);     
        aMap.put("GL", 7);
        aMap.put("OX", 7);
        
        aMap.put("NN", 8);
        aMap.put("MK", 8);
        aMap.put("LU", 8);
        aMap.put("AL", 8);
        aMap.put("SG", 8); 
        
        aMap.put("CO", 9);
        aMap.put("CB", 9);
        aMap.put("IP", 9);
        
        aMap.put("PE", 10);
        aMap.put("NR", 10);
        
        aMap.put("DA", 11);
        aMap.put("BR", 11);     
        aMap.put("CR", 11);       
        aMap.put("SE", 11);
        
        aMap.put("KT", 12);
        aMap.put("TW", 12);
        aMap.put("SM", 12);
        aMap.put("SW", 12); 
        
        aMap.put("UB", 13);
        aMap.put("W", 13);    
        aMap.put("WC", 13);
        
        aMap.put("WD", 14);
        aMap.put("HA", 14);        
        aMap.put("NW", 14);
        
        aMap.put("EN", 15);     
        aMap.put("N", 15);
        aMap.put("E", 15);
        aMap.put("EC", 15);
        
        aMap.put("NP", 16);
        aMap.put("SA", 16);
        aMap.put("CF", 16);
        aMap.put("HR", 16);  
        
        aMap.put("LD", 17);
        aMap.put("SY", 17);
        
        aMap.put("WR", 18);        
        aMap.put("DY", 18);
        aMap.put("B", 18);
        aMap.put("CV", 18);
        
        aMap.put("TF", 19);        
        aMap.put("ST", 19);
        aMap.put("WS", 19);
        aMap.put("DE", 19);
        aMap.put("WV", 19); 
        
        aMap.put("LE", 20);
        aMap.put("NG", 20);
        
        aMap.put("LL", 21);
        aMap.put("CH", 21);         
        aMap.put("CW", 21);        
               
        aMap.put("S", 22); 
        aMap.put("SK", 22);
        
        aMap.put("DN", 23);
        aMap.put("HU", 23);
        aMap.put("LN", 23); 
        
        aMap.put("L", 24);        
        aMap.put("WN", 24);
        aMap.put("WA", 24);
        aMap.put("M", 24);
        
        aMap.put("FY", 25);
        aMap.put("PR", 25);
        aMap.put("BL", 25);     
        aMap.put("BB", 25);
        
        aMap.put("OL", 26);
        aMap.put("HX", 26);
        aMap.put("HD", 26);
        aMap.put("WF", 26);
        aMap.put("LS", 26);
        
        aMap.put("BD", 27);     
        aMap.put("HG", 27);
        aMap.put("YO", 27);
        
        aMap.put("CA", 28);
        aMap.put("LA", 28);
        
        aMap.put("DL", 29);        
        aMap.put("TS", 29);
        aMap.put("DH", 29);     
        aMap.put("SR", 29);
        
        aMap.put("NE", 30);
        aMap.put("TD", 30);
        
        aMap.put("KA", 31);
        aMap.put("DG", 31);
        aMap.put("ML", 31);
        
        aMap.put("EH", 32);        
        aMap.put("KY", 32);
        
        aMap.put("FK", 33);
        aMap.put("G", 33);
        aMap.put("PA", 33);
        
        aMap.put("AB", 34);
        aMap.put("DD", 34);
        
        aMap.put("ZE", 35);     
        aMap.put("KW", 36);              
        aMap.put("BT", 37);
        aMap.put("HS", 38);        
        aMap.put("IV", 39);
        aMap.put("PH", 40);
        
        lookupTable = Collections.unmodifiableMap(aMap);
	}
	
	public Map<String, Integer> getLookupTable(){
		return lookupTable;
	}

}
