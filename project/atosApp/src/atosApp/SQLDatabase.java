package atosApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLDatabase {
	
	private static Connection conn = null;

	public SQLDatabase() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		conn = DriverManager.getConnection
		   ("jdbc:mysql://localhost:3306/atos1", "root", "abc12345");
	}
	
	public Connection getConn() throws SQLException{
		return conn;
	}
	
	public void closeConn() throws SQLException{
		conn.close();
	}

}
