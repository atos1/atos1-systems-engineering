package atosApp;


public class PatientAppointment {
	private String id;
	private String postCode;
	private String gender;
	private String sexPref;
	
	public PatientAppointment(String id, String postCode, String gender, String sexPref){
		this.id = id;
		this.postCode = postCode;
		this.gender = gender;
		this.sexPref = sexPref;
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getPostCode(){
		return this.postCode;
	}
	
	public String getGender(){
		return this.gender;
	}
	
	public String getPref(){
		return this.sexPref;
	}

}
