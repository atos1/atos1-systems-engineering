package atosApp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Region {

	private static Map<String, Integer> lookupTable = LookupTable.lookupTable;
	
	//Each region is mapped to a ArrayList containing all the clinicians in that region 
	public static HashMap<Integer, ArrayList<Clinician>> getMappedClinicianRegions(ResultSet resultSet){
		HashMap<Integer, ArrayList<Clinician>> map = new HashMap<Integer, ArrayList<Clinician>>(); 
		
		try {
			// Iterates over entire resultSet and adds locations to their designated regions.
			while(resultSet.next()) {
				String id = resultSet.getString(1);
				String fullPostCode = resultSet.getString(2);
				
				String postCodePrefix = getPostCodePrefix(fullPostCode);
				Integer region = lookupTable.get(postCodePrefix);
				String gender = resultSet.getString(3);
				addToClinicianRegion(map, region, id, fullPostCode, gender);
			}
		} catch (SQLException e) {
		    System.out.println("Unable to iterate over resultset");
		} 
		return map;
	}
	
	
	//Note: Each region is mapped to an ArrayList containing all the clinician in that region.
	//The method adds another clinician to the ArrayList that the passed region is the key to.
	public static void addToClinicianRegion(HashMap<Integer, ArrayList<Clinician>> map, 
													Integer region,	String id, String fullPostCode, String gender) {
		ArrayList<Clinician> appointmentsInRegion = map.get(region);
	
	    if(appointmentsInRegion == null) {
	    	
	         appointmentsInRegion = new ArrayList<Clinician>();
	         appointmentsInRegion.add(new Clinician(id, fullPostCode, gender));
	         map.put(region, appointmentsInRegion);
	         
	    } else {
	    	
	         appointmentsInRegion.add(new Clinician(id, fullPostCode, gender));
	    }
	}
	
	
	//Each region is mapped to a ArrayList containing all the appointments in that region 
	public static HashMap<Integer, ArrayList<PatientAppointment>> getMappedAppointmentRegions(ResultSet resultSet){
		
		HashMap<Integer, ArrayList<PatientAppointment>> map = new HashMap<Integer, ArrayList<PatientAppointment>>(); 
		
		try {
			// Iterates over entire resultSet and adds locations to their designated regions.
			while(resultSet.next()) {
				
				String id = resultSet.getString(1);
				String fullPostCode = resultSet.getString(2);
				
				String postCodePrefix = getPostCodePrefix(fullPostCode);
				Integer region = lookupTable.get(postCodePrefix);
				String gender = resultSet.getString(3);
				String sexPref = resultSet.getString(4);
				addToPatientAppointmentRegion(map, region, id, fullPostCode, gender, sexPref);
			}
			
		} catch (SQLException e) {
			
		    System.out.println("Unable to iterate over resultset");
		} 
		return map;
	}
	
	
	//Note: Each region is mapped to an ArrayList containing all the appointments in that region.
	//The method adds another appointment to the ArrayList that the passed region is the key to.
	public static void addToPatientAppointmentRegion(HashMap<Integer, ArrayList<PatientAppointment>> map, 
													Integer region, String id, String fullPostCode, String gender, String sexPref) {
		ArrayList<PatientAppointment> appointmentsInRegion = map.get(region);
	
	    if(appointmentsInRegion == null) {
	    	
	         appointmentsInRegion = new ArrayList<PatientAppointment>();
	         appointmentsInRegion.add(new PatientAppointment(id, fullPostCode, gender, sexPref));
	         map.put(region, appointmentsInRegion);
	         
	    } else {
	    	
	         appointmentsInRegion.add(new PatientAppointment(id, fullPostCode, gender, sexPref));
	    }
	}
	
	
	// Makes an array of the regions to process (from the appointment locations).
	public static <T> ArrayList<Integer> getRegionsInBatch(HashMap<Integer, ArrayList<T>> patientMap){
		
		ArrayList<Integer> returnedRegions = new ArrayList<Integer>();

		final Set<Entry<Integer, ArrayList<T>>> entries = patientMap.entrySet();
		for (Entry<Integer, ArrayList<T>> entry : entries) {
		    Integer region = entry.getKey();
		    returnedRegions.add(region);
		}
		return returnedRegions;
	}
	
	
	
	//Returns the post code prefix of a full post code, i.e. WC1 2HS returns WC
	public static String getPostCodePrefix(String postCode){
		int length = 0;
		postCode = postCode.trim();
		for (int i = 0; i < postCode.length(); i++){
			if (!Character.isLetter(postCode.charAt(i)))
				break;
			else
				length++;
		}
		return postCode.substring(0, length);
	}
	
	
}

//1. Go through complete batch of appointments
//2. Sort them into regions i.e. Region 1: appointments a,b,c,d, Region 2: appointments x,y,z

//   For each region: 
//3. Compute the travelling salesman algorithm API call to retrieve location sequence
//4. Iterate through clinicians in the same region for free slots to assign
//5. If appointment not allocated put into "waiting list"

//6. Assign appointments in waiting list 
