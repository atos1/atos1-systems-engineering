package apiMapQuest;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import com.mysql.jdbc.Connection;

import apiMapQuest.MapQuestAPI;
import atosApp.*;

public class Controller {

	private static SQLDatabase newConn = null;
	private static Connection conn = null;
		
	static String patientsQuery = "SELECT `sample_appointments`.`id`, `sample_appointments`.`post_code`,"
			+ "`sample_appointments`.`gender`, `sample_appointments`.`sex_pref`"
			+ "FROM `sample_appointments`";
	static String cliniciansQuery = "SELECT `clinicians`.`id`, `clinicians`.`post_code`, "
			+ "`clinicians`.`gender`"
			+ "FROM `clinicians`";
	
	
	public static void run() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException{
		
	    long startTime = System.currentTimeMillis();
		
		newConn = new SQLDatabase();
		conn = (Connection) newConn.getConn();
		Statement stmtP = conn.createStatement();
		Statement stmtC = conn.createStatement();
		// Retrieve result set from SQL queries of patients and clinicians
		ResultSet patientSet = stmtP.executeQuery(patientsQuery);
		ResultSet clinicianSet = stmtC.executeQuery(cliniciansQuery);

		// -  Each Integer in the HashMap represents a region that maps to all the PatientAppointments/Clinicians in that region 
		// 1. Creates a PatientAppointment or Clinician object for each patient or clinician in the ResultSet.
		// 2. And stores the object in an ArrayList containing all PatientAppointments/Clinicians in the same region.
		// Refer to Region.getMappedAppointmentRegions() on further detail on how this works.
		HashMap<Integer, ArrayList<PatientAppointment>> patientMap = Region.getMappedAppointmentRegions(patientSet); 		
		HashMap<Integer, ArrayList<Clinician>> clinicianMap = Region.getMappedClinicianRegions(clinicianSet);
		
		stmtP.close();
		stmtC.close();
		
		// Creates ArrayLists of all the regions to process
		// i.e if patientMap has regions (hashes/Integers) 1,2,4,7 then regionsInPatientBatch = [1,2,4,7]
		ArrayList<Integer> regionsInPatientBatch = Region.getRegionsInBatch(patientMap);	
		ArrayList<Integer> regionsInClinicianBatch = Region.getRegionsInBatch(clinicianMap);
		PrintStatement.regionsPerBatch(regionsInPatientBatch, regionsInClinicianBatch);

		// Assign random schedules to all clinicians
		ScheduleGenerator.assignAllSchedules(clinicianMap);
			    
		// Creating directory for excel workbook
		final File filepath = ExcelSchedule.getFilepath();
		XSSFWorkbook workbook = new XSSFWorkbook();
		
		// Unassigned appointments
		ArrayList<PatientAppointment> unassigned = new ArrayList<PatientAppointment>();
		// Unassigned appointments HashMap mapping regions to unassigned patients in that region
		HashMap<Integer, ArrayList<PatientAppointment>> unassignedPatientMap = new HashMap<Integer, ArrayList<PatientAppointment>>();
		
		// Storing percentage success rates
		ArrayList<Double> successRates = new ArrayList<Double>();
		
		// Number of regions to process
		int regionNumber = regionsInPatientBatch.size();
//		regionNumber = 5; // To avoid using up too many API transactions. Remove statement for live program.
		
		// Iterating through each region in the appointments batch
		for(int i = 0; i < regionNumber; i++){

			// Retrieves the i'th region in regionsInPatientBatch to process.
			int currentRegion = regionsInPatientBatch.get(i);

			System.out.println("Now in region " + currentRegion);
			// Retrieves all the PatientAppointments in the current region.
		    ArrayList<PatientAppointment> appointmentsInRegion = patientMap.get(currentRegion);
			
			// To store all of the Clinicians in the same region
			ArrayList<Clinician> cliniciansInRegion = new ArrayList<Clinician>();
			
			// Checks if there are any Clinicians in the same region,
			// If not: Add all appointments from the current region to "unassigned list" and print to excel workbook
			// 		   Then skip to next region (next iteration of the for loop)
			if ( regionsInClinicianBatch.contains(currentRegion) ){
				
				// Retrieves all the Clinicians in the same region
				cliniciansInRegion = clinicianMap.get(currentRegion);
				// Prints all the Clinicians in the same region on the console.
				PrintStatement.clinicianSlots(cliniciansInRegion);

				// Calls the API to compute travelling salesman solution on PatientAppointment locations (post codes)
				MapQuestAPI result = new MapQuestAPI(appointmentsInRegion);
				
				// 1. Schedules appointments to Clinicians and creates a list for unassigned appointments
				// 2. Creates an Excel sheet for the current region and does the appointment scheduling.
				// 3. Calculates the "success rate" of scheduling appointments for the current region.
				// 4. Prints out schedule for each Clinician to Excel sheet and also overall "success rate".
				AppointmentScheduler appointmentScheduler =  
						new AppointmentScheduler(cliniciansInRegion, appointmentsInRegion, workbook, result);
				
				// Retrieves unassigned appointments from the current region.
				ArrayList<PatientAppointment> unassignedAppointments = appointmentScheduler.getUnassignedAppointments();
				// Prints all the unassigned PatientAppointment IDs in the same region on the console.
				PrintStatement.appointmentIDs(unassignedAppointments);
				// Adds unassigned appointments to regional set of unassignment appointments in HashMap
				if (unassignedAppointments.size() != 0)
					unassignedPatientMap.put(currentRegion, unassignedAppointments);
				
				// Adds the unassigned appointments from the current region to the ArrayList containing all unassigned appointments.
				unassigned.addAll(unassignedAppointments);
				
				// Adds the "success rate" of the current region to an ArrayList containing all the success rates of all regions.
				successRates.add(appointmentScheduler.getSucessRate());
				System.out.println("\n");
				
			}
			else {
				
				// 1. Adds all appointments from the current region that has no Clinicians to an "unassigned list" 
				// 2. Prints unassigned appointments to Excel sheet for the current region.
				AppointmentScheduler appointmentScheduler = new AppointmentScheduler(appointmentsInRegion, currentRegion, workbook);
				
				// Retrieves unassigned appointments.
				ArrayList<PatientAppointment> unassignedAppointments = appointmentScheduler.getUnassignedAppointments();
				
				// Prints unassigned appointment IDs to console.
				PrintStatement.appointmentIDs(unassignedAppointments);
				
				// Adds unassigned appointments to an ArrayList containing the unassigned appointments of all regions.
				unassigned.addAll(unassignedAppointments);
				// Adds unassigned appointments to regional set of unassignment appointments in HashMap
				unassignedPatientMap.put(currentRegion, unassignedAppointments);

				continue;
			}
					
		}
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime; 
	    
	    // Prints to console all unassigned appointments
		PrintStatement.appointmentIDs(unassigned);
		
		// Prints final statics to a new sheet in the workbook
		// Contains all unassigned appointment IDs and average success rate and total time elapsed.
		ExcelSchedule.insertFinalStats(workbook, unassigned, successRates, elapsedTime);
		ExcelSchedule.publishFile(workbook, filepath);
		

		System.out.println("\n\n\n");
		
		
		
		
		
		
	    /* This is a rerun of the procedure above but with the unassigned appointments! */
	    // Here you can decide to run an identical loop on the unassigned appointments. on unassignedPatientMap
		// All previously used objects are reinitialised.
		// Using a method to encapsulate both would be difficult since the unassignedPatientMap cannot be replaced.
		XSSFWorkbook unassignedApptWorkbook = new XSSFWorkbook();
		ArrayList<Integer> regionsInUnassignedPatientBatch = Region.getRegionsInBatch(unassignedPatientMap);
		successRates = new ArrayList<Double>();
		regionNumber = regionsInUnassignedPatientBatch.size();
		unassigned = new ArrayList<PatientAppointment>();
		
		for(int i = 0; i < regionNumber; i++){

			// Retrieves the i'th region in regionsInPatientBatch to process.
			int currentRegion = regionsInUnassignedPatientBatch.get(i);

			System.out.println("Now in region " + currentRegion);
			// Retrieves all the PatientAppointments in the current region.
		    ArrayList<PatientAppointment> appointmentsInRegion = unassignedPatientMap.get(currentRegion);
			
			// To store all of the Clinicians in the same region
			ArrayList<Clinician> cliniciansInRegion = new ArrayList<Clinician>();
			
			// Checks if there are any Clinicians in the same region,
			// If not: Add all appointments from the current region to "unassigned list" and print to excel workbook
			// 		   Then skip to next region (next iteration of the for loop)
			if ( regionsInClinicianBatch.contains(currentRegion) ){
				
				// Retrieves all the Clinicians in the same region
				cliniciansInRegion = clinicianMap.get(currentRegion);
				// Prints all the Clinicians in the same region on the console.
				PrintStatement.clinicianSlots(cliniciansInRegion);

				// Calls the API to compute travelling salesman solution on PatientAppointment locations (post codes)
				MapQuestAPI result = new MapQuestAPI(appointmentsInRegion);
				
				// 1. Schedules appointments to Clinicians and creates a list for unassigned appointments
				// 2. Creates an Excel sheet for the current region and does the appointment scheduling.
				// 3. Calculates the "success rate" of scheduling appointments for the current region.
				// 4. Prints out schedule for each Clinician to Excel sheet and also overall "success rate".
				AppointmentScheduler appointmentScheduler =  
						new AppointmentScheduler(cliniciansInRegion, appointmentsInRegion, unassignedApptWorkbook, result);
				
				// Retrieves unassigned appointments from the current region.
				ArrayList<PatientAppointment> unassignedAppointments = appointmentScheduler.getUnassignedAppointments();
				// Prints all the unassigned PatientAppointment IDs in the same region on the console.
				PrintStatement.appointmentIDs(unassignedAppointments);
				
				// Adds the unassigned appointments from the current region to the ArrayList containing all unassigned appointments.
				unassigned.addAll(unassignedAppointments);
				
				// Adds the "success rate" of the current region to an ArrayList containing all the success rates of all regions.
				successRates.add(appointmentScheduler.getSucessRate());
				System.out.println("\n");
				
			}
			else {
				
				// 1. Adds all appointments from the current region that has no Clinicians to an "unassigned list" 
				// 2. Prints unassigned appointments to Excel sheet for the current region.
				AppointmentScheduler appointmentScheduler = new AppointmentScheduler(appointmentsInRegion, currentRegion, unassignedApptWorkbook);
				
				// Retrieves unassigned appointments.
				ArrayList<PatientAppointment> unassignedAppointments = appointmentScheduler.getUnassignedAppointments();
				
				// Prints unassigned appointment IDs to console.
				PrintStatement.appointmentIDs(unassignedAppointments);
				
				// Adds unassigned appointments to an ArrayList containing the unassigned appointments of all regions.
				unassigned.addAll(unassignedAppointments);

				continue;
			}
					
		}
		PrintStatement.appointmentIDs(unassigned);
		ExcelSchedule.insertFinalStats(unassignedApptWorkbook, unassigned, successRates, elapsedTime);
		ExcelSchedule.publishFile(unassignedApptWorkbook, ExcelSchedule.getFilepath2());
		
		conn.close();
	}
}