package apiMapQuest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import atosApp.*;

public class MapQuestAPI extends RouteOptimiserAPI{
	
	private String API_KEY = "Irpq3KobwWwKAu3atxXrFcAc3YBS48cQ";
	private HashMap<Integer, PatientAppointment> unoptimisedPatientApptObjects = new HashMap<Integer, PatientAppointment>();
	private ArrayList<PatientAppointment> optimisedPatientApptObjects = new ArrayList<PatientAppointment>();
	private String apiResponse;
	private ArrayList<Integer> apiLocationSequence = new ArrayList<Integer>();
	private ArrayList<Long> IDSequence = new ArrayList<Long>();
	private ArrayList<LocalTime> journeyTimes = new ArrayList<LocalTime>();
	
	// Executes api call for the passed in region
	public MapQuestAPI(ArrayList<PatientAppointment> appointmentsInRegion) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		// This is simply to record how long the API processing takes (not entirely necessary)
	    long startTime = System.currentTimeMillis();

		System.out.println("Patients in region: " + appointmentsInRegion.size());
//		this.unoptimisedPatientIDOrder = new HashMap<Integer, Long>();

		// If there is just one appointment in the list we do not need to carry out an API call.
		// Hence, we have the return statement enclosed within this if statement.
		// So the JSON request string parsing, API calling is skipped.
		if (appointmentsInRegion.size() == 1){
			// Add the only appointment in the region to the apiLocationSequence.
			this.apiLocationSequence.add(0);
			this.optimisedPatientApptObjects.add(appointmentsInRegion.get(0));
			return;
		}
		
		// The code only reaches here if there is more than 1 Patient in appointmentsInRegion/unoptimisedPatientApptObjects 
		// The Json request string is built here.
		StringBuilder jsonStringBuilder = new StringBuilder();
		jsonStringBuilder.append("{locations:[");
		int index = 0;

		for ( PatientAppointment appointment : appointmentsInRegion ){
			// The request string format is created here in accordance to the format required for a success API call as specified by the MapQuest API.
			// So for each location we simply just use its post code as this is of reasonable enough precision. 
			// Using geocoding (longitudes and latitudes) will take much longer to compute and that level of precision isn't necessary.
			jsonStringBuilder.append("{postalCode:\""+appointment.getPostCode()+"\"},");
			// HashMap is so we can find out the PatientAppt objects for the returned API sequence i.e. [2,3,1,4,0]
			// This way a record is maintained for the original sequence passed into the JSON parameter.
			// So we will know what PatientAppt objects location 2,3,1,4 and 0 refer to. 
			unoptimisedPatientApptObjects.put(index, appointment);
			index++;
			if(index == 25) /// NEED TO DO SOMETHING ABOUT THIS!
				break;
		}
		
		// We append the first PatientAppt to the end of the api request so that the beginning and end of the sequence is the same
		// This way the API will actually compute an actual solution for traversing all the locations.
		// *We do this, as by default, the API computes the fastest way of going from the first specified location to the last.*
		// So the locations in the request parameter will always end with the first appointment so: 0,1,2,3,4,5,0
		// If we didn't do this, we can have a situation like this (where the request instead is just: 0,1,2,3,4,5):
		// 		- Location 5 can be the closest to location 0, but the API will force it to be the last visited, 
		//		  so you wouldn't have the shortest path.
		for ( PatientAppointment appointment : appointmentsInRegion ){
			jsonStringBuilder.append("{postalCode:\""+appointment.getPostCode()+"\"},");
			break;
		}
		
		// These request parameters are included as we do not need directions from one place to another.
		jsonStringBuilder.append("],options:{narrativeType=none, manMaps=false, doReverseGeocode=false}}");
		String jsonParameterInput = jsonStringBuilder.toString().replaceAll(" ", "");
		System.out.println(jsonParameterInput);
		
		// This function actually execute the MapQuest API call.
	    this.executeApiCall(jsonParameterInput);
	    
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("Total api call time: " + elapsedTime);
	}
	
	
	// Executes api call with json parameter and returns location sequence.
	public void executeApiCall(String jsonParameter) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
	      
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		
		try {
			// Here the API key and jsonParameter is used to form the entire request URL.
			URL url = new URL("http://www.mapquestapi.com/directions/v2/"
					+ "optimizedroute?key="+ API_KEY +"&json="
					+ jsonParameter);
			// No need to explain the rest. Standard method of making HTTP Requests. 
			urlConn = url.openConnection();
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(),
						Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					String cp;
					while ((cp = bufferedReader.readLine()) != null) {
						sb.append(cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:"+  e);
		} 
		// Returned JSON response having completed the API call. 	
		this.apiResponse = sb.toString();
		
		// The journey times are parsed/extracted from the API response. 
	    this.journeyTimes = extractTravelTimes(apiResponse);
	    System.out.println(apiResponse);
	    // The optimised location sequence are extracted from the API response. This returns [2,3,5,1,4,0] for example. 
		this.apiLocationSequence = this.extractLocationSequence(apiResponse);
		// Sets optimised ID sequence and ArrayList of optimisedPatientApptObjects. 
	    setPatientAppointmentSequence();
	}
	
	// 
	public String getApiResponse(){
		return this.apiResponse;
	}
	
	// i.e. [2,4,6,1,3,5]
	public ArrayList<Integer> getApiLocationSequence(){
		return this.apiLocationSequence;
	}

	
	public HashMap<Integer, PatientAppointment> getUnoptimisedPatientApptObjects(){
		return this.unoptimisedPatientApptObjects;
	}		
	
	// Returns location sequence but with IDs instead of apiLocationSequence indexes.
	public ArrayList<Long> getIDSequence(){
		return this.IDSequence;
	}
	
	// Returns duration of getting from one location to another in order of ID sequence.
	public ArrayList<LocalTime> getJourneyTimes(){
		return this.journeyTimes;
	}
	
	// Returns patients in region in optimised sequence order.
	public ArrayList<PatientAppointment> getOptimisedPatientApptObjects(){
		return this.optimisedPatientApptObjects;
	}
		
	
	// Sets ID sequence and optimisedPatientApptObject ArrayList. 
	public void setPatientAppointmentSequence(){
		
		ArrayList<Long> optimisedIDSequence = new ArrayList<Long>();
		ArrayList<PatientAppointment> optimisedPatientApptObjects = new ArrayList<PatientAppointment>();
		int length = getApiLocationSequence().size();
		
		for(int i = 0; i < length; i++){
			// Retrieves the i'th value in the location sequence returned by the API. i.e. [1,4,3,0,2]
			// For example when i = 0, current = 1.
			int current = getApiLocationSequence().get(i);
			
			// Uses the HashMap created in the constructor to retrieve that Patient's ID/Object.
			optimisedIDSequence.add(Long.parseLong(getUnoptimisedPatientApptObjects().get(current).getID()));
			optimisedPatientApptObjects.add(getUnoptimisedPatientApptObjects().get(current));
		}
		this.IDSequence = optimisedIDSequence;
		this.optimisedPatientApptObjects = optimisedPatientApptObjects;
	}
	
	// Takes JSON API response and extracts location sequence (optimised order) inyo an ArrayList.
	public ArrayList<Integer> extractLocationSequence (String jsonResponse){
		// I'm simply finding where the location sequence starts and where it ends
		jsonResponse = jsonResponse.replaceAll("\"", "%");
	    int position = jsonResponse.lastIndexOf("locationSequence");
	    
		// This is where it begins i.e. 19 indexes after "locationSequence" in the API response string.
	    jsonResponse = jsonResponse.substring(position+19);
	    // This is where it ends when it sees a ']'.
	    position = jsonResponse.indexOf("]");
	    // jsonResponse is now equal to the string "[0,1,3,2,0]" (a string including the commas)
	    jsonResponse =  jsonResponse.substring(0, position);
	    // A string array is created containing each of the numbers. (, is removed to do this)
	    String[] strArray = jsonResponse.split(",");
	    ArrayList<Integer> optimisedOrder = new ArrayList<Integer>();
	    
	    // We only go up until strArray.length-1 as the last index will always be 0. (We do not need 2 0s).
	    for(int i = 0; i < strArray.length - 1; i++) { 
	    	optimisedOrder.add(Integer.parseInt(strArray[i]));
	    }
	    // We now have an Integer array: [0,1,3,2]

	    System.out.println(optimisedOrder);
	    
	    // True if its faster having the first destination be reached last we turn 
	    // If so, [0,1,3,2] becomes [1,3,2,0], otherwise it remains as [0,1,3,2]
	    if (this.journeyTimes.get(0).compareTo(this.journeyTimes.get(this.journeyTimes.size()-1)) > 0){
	    	this.journeyTimes.remove(0);
	    	optimisedOrder.remove(0);
	    	optimisedOrder.add(0); // to the end
	    }else{ 
	    	this.journeyTimes.remove(journeyTimes.size()-1);
	    }
	    this.journeyTimes.add(0, LocalTime.of(0, 0, 0));
	    
	    System.out.println(journeyTimes);
	    System.out.println(optimisedOrder);

	    return optimisedOrder;
	}


	public ArrayList<LocalTime> extractTravelTimes(String jsonResponse){
		// Parses journey times from the apiResponse into an ArrayList.
		ArrayList<LocalTime> journeyTimes = new ArrayList<LocalTime>();	
		jsonResponse = jsonResponse.replaceAll("\"", "%");
		int i = jsonResponse.indexOf("destNarrative");
		while(i >= 0) {
			 // Each journey time is added to the journeyTimes ArrayList.
		     journeyTimes.add((LocalTime.parse(jsonResponse.substring(i-11, i-3))));
		     i = jsonResponse.indexOf("destNarrative", i+1);
		}
		
		System.out.println(journeyTimes);
		
		return journeyTimes;
	}

}
