package tests;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import atosApp.Clinician;
import atosApp.Region;
import atosApp.SQLDatabase;
import atosApp.Schedule;
import atosApp.ScheduleGenerator;

public class AssignSchedulesTest {

	
	SQLDatabase newConn = null;
	Connection conn = null;

	ResultSet patientSet, clinicianSet;

	String cliniciansQuery = "SELECT `clinicians`.`id`, `clinicians`.`post_code`, "
			+ "`clinicians`.`gender`"
			+ "FROM `clinicians`";
	HashMap<Integer, ArrayList<Clinician>> clinicianMap;

	@Before
	public void databaseSetup() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		newConn = new SQLDatabase();
		conn = (Connection) newConn.getConn();
		Statement stmtC = conn.createStatement();
		// Retrieve result set from SQL queries of patients and clinicians
		clinicianSet = stmtC.executeQuery(cliniciansQuery);
		
		clinicianMap = Region.getMappedClinicianRegions(clinicianSet); 
	}


	
	@Test
	public void test2() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		boolean check = true;
		ArrayList<Clinician> clinicians = clinicianMap.get(0);
		Schedule s2 = new Schedule(); //no free slots
		s2.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s2.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s2.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));

		ScheduleGenerator.assignAllSchedules(clinicianMap);

		
		assertEquals(check,true);
	}
	
	
	@Test
	public void test3() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		boolean check = true;
		ArrayList<Clinician> clinicians = clinicianMap.get(2);
		Schedule s1 = new Schedule(); //no free slots
		s1.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s1.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s1.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));

		ScheduleGenerator.assignAllSchedules(clinicianMap);

		
		assertEquals(check,true);
	}
	
	@Test
	public void test4() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		boolean check = true;
		ArrayList<Clinician> clinicians = clinicianMap.get(4);
		Schedule s3 = new Schedule(); //no free slots
		s3.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s3.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s3.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));

		ScheduleGenerator.assignAllSchedules(clinicianMap);

		
		assertEquals(check,true);
	}
	@Test
	public void test5() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		boolean check = true;
		ArrayList<Clinician> clinicians = clinicianMap.get(6);
		Schedule s1 = new Schedule(); //no free slots
		s1.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s1.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s1.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));

		ScheduleGenerator.assignAllSchedules(clinicianMap);

		
		assertEquals(check,true);
	}
	@Test
	public void test6() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		boolean check = true;
		ArrayList<Clinician> clinicians = clinicianMap.get(8);
		Schedule s4 = new Schedule(); //no free slots
		s4.addTimeSlot(LocalTime.of(8,0,0), LocalTime.of(10,5,0));
		s4.addTimeSlot(LocalTime.of(12,5,0), LocalTime.of(14,40,0));
		s4.addTimeSlot(LocalTime.of(16,0,0), LocalTime.of(18,0,0));

		ScheduleGenerator.assignAllSchedules(clinicianMap);

		
		assertEquals(check,true);
	}
	
	
	
	
}
