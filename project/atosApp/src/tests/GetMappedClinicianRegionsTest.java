package tests;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import atosApp.Clinician;
import atosApp.PatientAppointment;
import atosApp.Region;
import atosApp.SQLDatabase;

public class GetMappedClinicianRegionsTest {
	
	SQLDatabase newConn = null;
	Connection conn = null;
	ResultSet patientSet, clinicianSet;
	
	@Before
	public void databaseSetup() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		newConn = new SQLDatabase();
		conn = (Connection) newConn.getConn();
	}

	@Test
	public void testPatientQuery() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{


		String cliniciansQuery = "SELECT `clinicians`.`id`, `clinicians`.`post_code`, "
				+ "`clinicians`.`gender`"
				+ "FROM `clinicians`";
		
		Statement stmtC = conn.createStatement();
		// Retrieve result set from SQL queries of patients and clinicians
		clinicianSet = stmtC.executeQuery(cliniciansQuery);
		
		HashMap<Integer, ArrayList<Clinician>> clinicianMap = Region.getMappedClinicianRegions(clinicianSet); 		
		
		assertEquals(clinicianMap.isEmpty(), false);

	}

}
