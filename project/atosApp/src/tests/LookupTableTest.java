package tests;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import atosApp.LookupTable;

public class LookupTableTest {
	
	private static Map<String, Integer> lookupTable;
	
	@org.junit.Before
	public void Before(){
		lookupTable = LookupTable.lookupTable;
	}

		public static String getPostCodePrefix(String postCode){
			int length = 0;
			postCode = postCode.trim();
			for (int i = 0; i < postCode.length(); i++){
				if (!Character.isLetter(postCode.charAt(i)))
					break;
				else
					length++;
			}
			return postCode.substring(0, length);
		}
	
	@Test
	public void test1(){
		String fullPostCode = "TQ12 1FC"; // 1
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 1;
		assertEquals(region, test);
	}
	
	@Test
	public void test2(){
		String fullPostCode = "BN2 2U2"; // 4
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 4;
		assertEquals(region, test);
	}
	
	@Test
	public void test3(){
		String fullPostCode = "SP2 2HD"; // 3
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 3;
		assertEquals(region, test);
	}
	
	@Test
	public void test4(){
		String fullPostCode = "CO3 2WC"; // 9
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 9;
		assertEquals(region, test);
	}
	
	@Test
	public void test5(){
		String fullPostCode = "UB7 3DE"; // 13
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 13;
		assertEquals(region, test);
	}
	
	@Test
	public void test6(){
		String fullPostCode = "E15 4LP"; // 15
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 15;
		assertEquals(region, test);
	}
	
	@Test
	public void test7(){
		String fullPostCode = "LD17 3HX"; // 17
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 17;
		assertEquals(region, test);
	}
	
	@Test
	public void test8(){
		String fullPostCode = "B2 2HD"; // 18
		String postCodePrefix = getPostCodePrefix(fullPostCode);
		Integer region = lookupTable.get(postCodePrefix);
		Integer test = 18;
		assertEquals(region, test);
	}
}
