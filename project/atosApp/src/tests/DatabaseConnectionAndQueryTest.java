package tests;
import static org.junit.Assert.assertEquals;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.mysql.jdbc.Connection;
import atosApp.SQLDatabase;
	
public class DatabaseConnectionAndQueryTest {
	
		SQLDatabase newConn = null;
		Connection conn = null;
		ResultSet patientSet, clinicianSet;
		
		@Before
		public void databaseSetup() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
			newConn = new SQLDatabase();
			conn = (Connection) newConn.getConn();
		}

		@Test
		public void testPatientQuery() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{

			String patientsQuery = "SELECT `sample_appointments`.`id`, `sample_appointments`.`post_code`,"
					+ "`sample_appointments`.`gender`, `sample_appointments`.`sex_pref`"
					+ "FROM `sample_appointments`";
			String cliniciansQuery = "SELECT `clinicians`.`id`, `clinicians`.`post_code`, "
					+ "`clinicians`.`gender`"
					+ "FROM `clinicians`";
			
			Statement stmtP = conn.createStatement();
			Statement stmtC = conn.createStatement();
			// Retrieve result set from SQL queries of patients and clinicians
			patientSet = stmtP.executeQuery(patientsQuery);
			assertEquals(patientSet.next(), true);

		}
		
		@Test
		public void testClinicianQuery() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
				
			String patientsQuery = "SELECT `sample_appointments`.`id`, `sample_appointments`.`post_code`,"
					+ "`sample_appointments`.`gender`, `sample_appointments`.`sex_pref`"
					+ "FROM `sample_appointments`";
			String cliniciansQuery = "SELECT `clinicians`.`id`, `clinicians`.`post_code`, "
					+ "`clinicians`.`gender`"
					+ "FROM `clinicians`";
			
			Statement stmtP = conn.createStatement();
			Statement stmtC = conn.createStatement();

			clinicianSet = stmtC.executeQuery(cliniciansQuery);
			assertEquals(clinicianSet.next(), true);
		}
		
		
		
	}


