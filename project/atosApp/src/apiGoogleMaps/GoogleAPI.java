package apiGoogleMaps;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import atosApp.RouteOptimiserAPI;
import atosApp.PatientAppointment;

public class GoogleAPI extends RouteOptimiserAPI {
	
//	"https://maps.googleapis.com/maps/api/directions/"+ type +"?origin=London,UK&destination=London,UK&waypoints=optimize:true|UB82FR|SE78PX|SW1X0BJ|UB83SD|W37AD|EC1M4NH|TW26RD|N182XF|SE231QB|SE172NT|BR20EL|NW104AG|IG45PX|EC1R4PX|E174ET|E176GR|W149QD|E70JA|E162JF|UB40TU|W104AT|SE265SD|&key=AIzaSyDpnYwaZeWKku0S6trpfBf6siityLOG92s";
	private String urlRequest = "https://maps.googleapis.com/maps/api/directions/xml?";
	private String API_KEY = "&key=AIzaSyDpnYwaZeWKku0S6trpfBf6siityLOG92s"; 
//	private String API_KEY = "&key=AIzaSyBtoDSYy5DbSwXSsdRSw6DwM2U_9GntAcA";
	
	private HashMap<Integer, PatientAppointment> unoptimisedPatientApptObjects = new HashMap<Integer, PatientAppointment>();
	private ArrayList<PatientAppointment> optimisedPatientApptObjects = new ArrayList<PatientAppointment>();
	private String apiResponse;
	private ArrayList<Integer> apiLocationSequence = new ArrayList<Integer>();
	private ArrayList<Long> IDSequence = new ArrayList<Long>();
	private ArrayList<LocalTime> journeyTimes = new ArrayList<LocalTime>();
	
	// Executes api call for the passed in region
	public GoogleAPI(ArrayList<PatientAppointment> appointmentsInRegion) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		// This is simply to record how long the API processing takes (not entirely necessary)
	    long startTime = System.currentTimeMillis();

		System.out.println("Patients in region: " + appointmentsInRegion.size());
			
		StringBuilder jsonStringBuilder = new StringBuilder();
		String originDest = appointmentsInRegion.get(0).getPostCode();
		jsonStringBuilder.append("origin=" + originDest + "&destination=" + originDest);
		jsonStringBuilder.append("&waypoints=optimize:true|");
		int index = 0;
			
		for ( PatientAppointment appointment : appointmentsInRegion ){
			// The request string format is created here in accordance to the format required for a success API call as specified by the MapQuest API.
			// So for each location we simply just use its post code as this is of reasonable enough precision. 
			// Using geocoding (longitudes and latitudes) will take much longer to compute and that level of precision isn't necessary.
			jsonStringBuilder.append(appointment.getPostCode() + "|");
			// HashMaps are created so we can find out the PatientAppt objects for the returned API sequence i.e. [2,3,1,4,0]
			// This way a record is maintained for the original sequence passed into the JSON parameter.
			// So we will know what PatientAppt objects location 2,3,1,4 and 0 refer to. 
			unoptimisedPatientApptObjects.put(index, appointment);
			index++;
			if(index == 25) /// NEED TO DO SOMETHING ABOUT THIS!
				break;
		}
		
		// Unlike MapQuestAPI, the Google API automatically computes the fastest route of traversing all locations and not from 
		// just the first specified in the parameter to the last. 
		
		// Here the API key is added to the jsonParameter.
		jsonStringBuilder.append(this.API_KEY);
		String jsonParameterInput = jsonStringBuilder.toString().replaceAll(" ", "");
		
		// This function actually execute the Google Directions API call.
	    this.executeApiCall(jsonParameterInput);
	    
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("Total api call time: " + elapsedTime);
	}
	
	
	// Executes api call with json parameter and returns location sequence.
	public void executeApiCall(String jsonParameter) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
	      
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		
		try {
			// Here the jsonParameter is used to form the entire request URL.
			URL url = new URL(this.urlRequest + jsonParameter);
			urlConn = url.openConnection();
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(),
						Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					String cp;
					while ((cp = bufferedReader.readLine()) != null) {
						sb.append(cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:"+  e);
		} 
				      	      
		this.apiResponse = sb.toString();
		//System.out.println(this.apiResponse.replaceAll("\"", "%").replaceAll("\\\\", "*"));
		
		// The journey times are parsed/extracted from the API response. 
	    this.journeyTimes = extractTravelTimes(apiResponse);
	    // The optimised location sequence are extracted from the API response. This returns [2,3,5,1,4,0] for example. 
		this.apiLocationSequence = this.extractLocationSequence(apiResponse);
		// Sets optimised ID sequence and ArrayList of optimisedPatientApptObjects. 
	    setPatientAppointmentSequence();
	}
	
	// 
	public String getApiResponse(){
		return this.apiResponse;
	}
	
	// i.e. [2,4,6,1,3,5]
	public ArrayList<Integer> getApiLocationSequence(){
		return this.apiLocationSequence;
	}
	
	
	public HashMap<Integer, PatientAppointment> getUnoptimisedPatientApptObjects(){
		return this.unoptimisedPatientApptObjects;
	}	
	
	// Returns location sequence but with IDs instead of post codes.
	public ArrayList<Long> getIDSequence(){
		return this.IDSequence;
	}
	
	// Returns duration of getting from one location to another in order of ID sequence.
	public ArrayList<LocalTime> getJourneyTimes(){
		return this.journeyTimes;
	}
	
	// Returns patients in region in optimised sequence order.
	public ArrayList<PatientAppointment> getOptimisedPatientApptObjects(){
		return this.optimisedPatientApptObjects;
	}
		
	
	// Sets ID sequence and optimisedPatientApptObject ArrayList. 
	public void setPatientAppointmentSequence(){
		
		ArrayList<Long> optimisedIDSequence = new ArrayList<Long>();
		ArrayList<PatientAppointment> optimisedPatientApptObjects = new ArrayList<PatientAppointment>();
		int length = getApiLocationSequence().size();
		
		for(int i = 0; i < length; i++){
			// Retrieves the i'th value in the location sequence returned by the API. i.e. [1,4,3,0,2]
			// For example when i = 0, current = 1.
			int current = getApiLocationSequence().get(i);
			
			// Uses the HashMap created in the constructor to retrieve that Patient's ID/Object.
			optimisedIDSequence.add(Long.parseLong(getUnoptimisedPatientApptObjects().get(current).getID()));
			optimisedPatientApptObjects.add(getUnoptimisedPatientApptObjects().get(current));
		}
		this.IDSequence = optimisedIDSequence;
		this.optimisedPatientApptObjects = optimisedPatientApptObjects;
	}
	
	// Extracts and then converts json location sequence to an ArrayList
	// Takes JSON API response and extracts location sequence or optimised order.
	public ArrayList<Integer> extractLocationSequence (String apiResponse){

		ArrayList<Integer> optimisedOrder = new ArrayList<Integer>();
		// The sequence is found in "<waypoint_index>5</waypoint_index>" as 5.
		int i = apiResponse.indexOf("<waypoint_index>");
		int j = apiResponse.indexOf("</waypoint_index>");
		while(i >= 0) {
			 // i+16 refers to the index of 5, j refers to the index of the '<' immediately after.
		     optimisedOrder.add((Integer.parseInt(apiResponse.substring(i+16, j))));
		     i = apiResponse.indexOf("<waypoint_index>", i+1);
		     j = apiResponse.indexOf("</waypoint_index>", j+1);
		}
		System.out.println(optimisedOrder);
		
	    return optimisedOrder;
	}


	public ArrayList<LocalTime> extractTravelTimes(String apiResponse){
		// Parses journey times from the apiResponse into an ArrayList.
		ArrayList<LocalTime> journeyTimes = new ArrayList<LocalTime>();	
		String temp = apiResponse;
		int i = temp.indexOf("</leg>");
		int j = temp.lastIndexOf("<duration>", i);
		int k = temp.indexOf("</value>", j);

		while (i >= 0){
			// Finds each journey time which is represented by the Google Maps api in seconds
			// This is converted to the HH:MM:SS format and parsed into the journeyTimes ArrayList.
			int apiSeconds = Integer.parseInt(temp.substring(j+21, k));
			int seconds = (int) apiSeconds % 60 ;
			int minutes = (int) ((apiSeconds / 60) % 60);
			int hours   = (int) ((apiSeconds / (60*60)) % 24);
			journeyTimes.add(LocalTime.of(hours, minutes, seconds));
		    i = temp.indexOf("</leg>", i+1);
		    j = temp.lastIndexOf("<duration>", i);
		    k = temp.indexOf("</value>", j);
		}
		try {
			journeyTimes.remove(journeyTimes.size()-1);
		}
		catch (Exception e) {
			System.out.println("Error api call not executed: " + this.urlRequest + "\n" + this.apiResponse);
		}
		System.out.println(journeyTimes);
		return journeyTimes;
	}

}
