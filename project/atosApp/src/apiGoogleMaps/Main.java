package apiGoogleMaps;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
	    long startTime = System.currentTimeMillis();
	    // For JSON look for waypoint_order for optimal route look for 
	    // For XML look for waypoint_index for optimal route, look for <leg></leg> for each journey
	    try {
			Controller.run();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("Total:" + elapsedTime);	
	}

}
